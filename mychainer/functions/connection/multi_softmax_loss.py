import numpy

import chainer
from chainer import cuda
import chainer.functions as F
from chainer.utils import type_check
from mychainer.functions.array.selectsum import select_sum


def multi_softmax_loss(x, ts, aggregate_option='mean'):
    """

    score = - log( \sum_{i \in I} p(i) )

    """
    epsilon = 1e-30   # avoid p=0 (in case the number of classes is large, p can be numerically 0)
    batchsize = x.shape[0]

    if aggregate_option == 'mean':
        score = F.sum(- F.log(select_sum(F.softmax(x), ts) + epsilon)) / batchsize
    elif aggregate_option is None:
        score = - F.log(select_sum(F.softmax(x), ts) + epsilon)
    else:
        raise ValueError('Unknown Aggregation Option')

    return score
