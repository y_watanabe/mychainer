import binascii
import itertools
import os
import time

import numpy
import six

import cupy
import chainer
from chainer import cuda
from chainer import function
from chainer.functions.array import concat
from chainer.functions.array import reshape
from chainer.functions.array import split_axis
from chainer.functions.array import stack
from chainer.functions.connection import linear
from chainer.functions.noise import dropout
from chainer.utils import type_check
import chainer.functions as F
from chainer.functions.math.matmul import _matmul, _batch_matmul

def _split(inputs, pos):
    return inputs[:pos], inputs[pos:]


def _seqs_to_array(xs, length, pad_value):
    batchsize = len(xs)
    xp = cuda.get_array_module(*xs)
    dtype = xs[0].dtype
    unit = xs[0].shape[1:]
    outs = xp.full((batchsize, length) + unit, pad_value, dtype=dtype)

    if xp is numpy:
        for i, x in enumerate(xs):
            outs[i, :len(x), ...] = x

    else:
        offsets1 = numpy.empty(len(xs) + 1, dtype='i')
        offsets1[0] = 0
        numpy.cumsum([len(x) for x in xs], out=offsets1[1:])

        xsc = xp.concatenate(xs, axis=0)
        unit_size = xs[0].size // len(xs[0])
        size = length * batchsize * unit_size
        cuda.elementwise(
            'int32 len, int32 unit, raw int32 offsets1, raw T xsc',
            'raw T out',
            '''
            int ind = i / unit;
            int off = i - ind * unit;
            int y = ind / len;
            int x = ind - y * len;
            if (offsets1[y] + x < offsets1[y+1]){
              out[i] = xsc[(offsets1[y] + x) * unit + off];
            }
            ''',
            'seqs_to_array'
        )(length, unit_size, cuda.to_gpu(offsets1), xsc, outs, size=size)

    return outs


def _mask_array(array, lens, mask_value):
    """write mask values in place"""
    xp = cuda.get_array_module(array)

    if xp is numpy:
        for i, x in enumerate(array):
            array[i, lens[i]:] = mask_value

    else:
        maxlen = array.shape[1]
        lens = xp.array(lens).astype(xp.int32)

        cuda.elementwise(
            'T val, int32 len, raw int32 lens',
            'raw T array',
            '''
            int y = i / len;
            int x = i - y * len;
            if (lens[y] < x + 1){
              array[i] = val;
            }
            ''',
            'mask_array'
        )(mask_value, maxlen, cuda.to_gpu(lens), array, size=array.size)


class AttentionScoreXk(function.Function):
    """
    Similar to AttentionScoreExpDot (no softmax normalization)
    Not exp but x**k
    """
    def __init__(self, k):
        if not (isinstance(k, int) and k > 0):
            raise TypeError('k must be positive integer')
        self.k = k

    def check_type_forward(self, in_types):
        q_type = in_types[0]
        x_types = in_types[1:]

        type_check.expect(
            q_type.dtype == numpy.float32,
            q_type.ndim == 2,
            q_type.shape[0] == len(x_types),
        )

        dim = q_type.shape[1]
        for x_type in x_types:
            type_check.expect(
                x_type.dtype == numpy.float32,
                x_type.ndim == 2,
                x_type.shape[1] == dim,
            )

    def forward_cpu(self, inputs):
        (q,), inputs = _split(inputs, 1)
        x_list = inputs
        dim = q.shape[1]
        batchsize = len(x_list)
        lens = list(map(len, x_list))
        max_len = max(lens)
        xmat = _seqs_to_array(x_list, max_len, 0)

        # inner product scores
        dtype = q.dtype
        score = numpy.empty((batchsize, max_len), dtype=dtype)
        for i in six.moves.range(batchsize):
            score[i] = _matmul(xmat[i], q[i], transa=False, transb=False).ravel()

        # mask scores
        _mask_array(score, lens, 0)

        # exp
        if self.k == 1:
            self.score_km1 = 1
        else:
            self.score_km1 = numpy.power(score, self.k-1)
        self.score_k = self.score_km1 * score
        return tuple([a[:lens[i]] for (i, a) in enumerate(self.score_k)])

    def forward_gpu(self, inputs):
        (q,), inputs = _split(inputs, 1)
        x_list = inputs
        dim = q.shape[1]
        batchsize = len(x_list)
        lens = list(map(len, x_list))
        max_len = max(lens)
        xmat = _seqs_to_array(x_list, max_len, 0)

        #score = cupy.empty((batchsize, max_len, 1), dtype=xmat.dtype)
        score = _batch_matmul(xmat, q)
        score = score.reshape((batchsize, max_len))
        _mask_array(score, lens, 0)

        # no softmax
        if self.k == 1:
            self.score_km1 = 1
        else:
            self.score_km1 = cupy.power(score, self.k-1)
        score_k = score * self.score_km1
        return tuple([a[:lens[i]] for (i, a) in enumerate(score_k)])

    def backward_cpu(self, inputs, grads):
        q = inputs[0]
        x_list = inputs[1:]
        batchsize = len(x_list)
        lens = list(map(len, x_list))
        max_len = max(lens)
        xmat = _seqs_to_array(x_list, max_len, 0)
        gmat = _seqs_to_array(grads, max_len, 0)

        # gq
        _gs = self.k * self.score_km1 * gmat
        gq = numpy.sum(numpy.expand_dims(_gs, axis=2) * xmat, axis=1)

        # gx_list
        gx = numpy.expand_dims(_gs, axis=2) * numpy.expand_dims(q, axis=1)
        gx_list = [gxi[:l, :] for (gxi, l) in zip(gx, lens)]

        return tuple([gq, ] + gx_list)

    def backward_gpu(self, inputs, grads):
        q = inputs[0]
        x_list = inputs[1:]
        dim = q.shape[1]
        batchsize = len(x_list)
        lens = list(map(len, x_list))
        max_len = max(lens)
        xmat = _seqs_to_array(x_list, max_len, 0)
        gmat = _seqs_to_array(grads, max_len, 0)

        # gq
        _gs = self.k * self.score_km1 * gmat
        gq = numpy.sum(cupy.expand_dims(_gs, axis=2) * xmat, axis=1)

        # gx_list
        gx = cupy.expand_dims(_gs, axis=2) * cupy.expand_dims(q, axis=1)
        gx_list = [gxi[:l, :] for (gxi, l) in zip(gx, lens)]

        return tuple([gq, ] + gx_list)


def attention_score_xk(k, q, xs):
    return AttentionScoreXk(k)(q, *xs)
