import binascii
import itertools
import os
import time

import numpy
import six

import cupy
import chainer
from chainer import cuda
from chainer import function
from chainer.functions.array import concat
from chainer.functions.array import reshape
from chainer.functions.array import split_axis
from chainer.functions.array import stack
from chainer.functions.connection import linear
from chainer.functions.noise import dropout
from chainer.utils import type_check
import chainer.functions as F
from chainer.functions.math.matmul import _matmul, _batch_matmul
from .attention import attention, _seqs_to_array



def attention_gate(xs, zs):
    """
    This attention_gate function is a generalization of attention function.
    Vector q is generalized to sequence zs.

    Args:
        zs: list of chainer Variables.
            "question words" sequence
        xs: list of chainer Variables.
            "document words" sequence

    Returns:
        ds: list of chainer Variables.
        len(ds) == len(zs) == len(xs) == batchsize

        alpha[i][j][k] = softmax_k(xs[i][j]*zs[i][k])   # softmax over question words
        ds[i][j] = \sum_k alpha[i][k] zs[i][k]          # get question vector for each word position j.
    """
    x_lens = list(map(lambda x: x.shape[0], xs))
    x0 = F.concat(xs, axis=0)

    zss = []
    for z, l in zip(zs, x_lens):
        zss.extend([z] * l)

    v = attention(x0, zss)
    return F.split_axis(v, numpy.cumsum(x_lens)[:-1], axis=0)


