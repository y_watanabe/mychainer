import numpy
import six

import chainer
from chainer import cuda
from chainer import function
import chainer.functions as F
from chainer.utils import type_check
import cupy

# this funciton is analogous to `numpy.add.reduceat`
def reduceat(x, a):
    """
    Example
    x = array([ 0.1,  0.2,  0.3,  0.4,  1.5,  1.6,  2.7])
    a = array([0, 1, 3, 5, 7], dtype=int32)  # len = n+1
    y = np.zeros((4,))

    a[0] = 0
    a[-1] = len(x)
    """
    n = a.size - 1
    y = cupy.zeros((n,), dtype=x.dtype)
    cuda.elementwise(
        'raw T x, raw int32 a',
        'raw T y',
        '''
        int k = 0;
        for(k = a[i]; k < a[i+1]; k++){
            y[i] += x[k];
        }
        ''',
        'reduceat'
    )(x, a, y, size=y.size)
    return y


def xy_select(ix, iy, x):
    y = cuda.elementwise(
    'S ix, S iy, raw T x',
    'T y',
    '''
    int ind[] = {ix, iy};
    y = x[ind];
    ''',
    'xy_select'
    )(ix, iy, x)
    return y


class SelectSum(function.Function):
    """Select elements and sum over them"""

    def check_type_forward(self, in_types):
        pass

    def forward(self, inputs):
        x = inputs[0]
        t = inputs[1:]  # t is list of arrays
        xp = cuda.get_array_module(x)

        if xp is numpy:
            mask = xp.zeros_like(x)
            for k, ti in enumerate(t):
                for l in ti:
                    mask[k, int(l)] += 1
            return xp.sum(x * mask, axis=1),
        else:
            _a = [0]
            _ix = []
            for (k, ti) in enumerate(t):
                _ix.extend([k] * len(ti))
                _a.append(_a[-1] + len(ti))
            ix = cupy.array(_ix, dtype=cupy.int32)
            iy = cupy.concatenate(t, axis=0).astype(cupy.int32)
            a = cupy.array(_a, dtype=cupy.int32)
            s = xy_select(ix, iy, x)
            y = reduceat(s, a)
            return y,

    def backward_cpu(self, inputs, grad_outputs):
        x = inputs[0]
        t = inputs[1:]  # t is list of arrays
        gloss = grad_outputs[0]
        gx = numpy.zeros_like(x)
        for i, ti in enumerate(t):
            for u in ti:
                gx[i, u] += gloss[i]
        return (gx,) + (None,) * len(t)

    def backward_gpu(self, inputs, grad_outputs):
        x = inputs[0]
        t = inputs[1:]  # t is list of arrays
        gloss = grad_outputs[0]
        gx = cuda.cupy.zeros_like(x)
        n = x.shape[1]

        _a = [0]
        for (k, ti) in enumerate(t):
            _a.append(_a[-1] + len(ti))
        a = cupy.array(_a, dtype=cupy.int32)
        iy = cupy.concatenate(t, axis=0).astype(cupy.int32)

        cuda.elementwise(
            'int32 n, raw S iy, raw S a, raw T gloss',
            'raw T gx',
            '''
            int k = 0;
            for(k = a[i]; k < a[i+1]; k++){
                gx[n * i + iy[k]] += gloss[i];
            }
            ''',
            'selectsum_bwd'
        )(n, iy, a, gloss, gx, size=len(gx))

        return (gx,) + (None,) * len(t)


def select_sum(x, ts):
    """Select and sum
    y[i] = smu_j x[i, ts[i][j]]

    Args:
        x: chainer Variable
            x.shape == (batchsize, l)
        ts: list of chainer Variables
            len(ts) == batchsize
            0 <= ts[i][j] < l
    Returns:
        y: chainer Variable
            y.shape == (batchsize, l) 
    """
    return SelectSum()(*((x,) + tuple(ts)))


def select_sum2(xs, tss):
    """
    This function is used for pointer sum architecture.
    y[i] = sum(x[i][tij] for tij in t[i])    0<=zij<li

    Args:
        xs: list of sequences  (len(x)==batchsize, x[0].shape == (li,k) or (li,)  )
        tss: list of list of positions  (len(tss)==batchsize, len(tss[0])==num_entities, tss[0][0].shape == (p,): number of positions )
    Returns:
        ys: list of chainer Variables
            ys[i]
    
    """
    ret = []
    for xi, tsi in zip(xs, tss):
        k = len(tsi)
        _xi = F.concat([F.expand_dims(xi, 0)] * k, 0)
        yi = select_sum(_xi, tsi)
        ret.append(yi)
    return ret
