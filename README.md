My project's README
===================


Set Up
---------

```
$ export PYTHONPATH=/path/to/mychainer:$PYTHONPATH
```

Test
------

```
$ nosetests mychainer_tests/
```