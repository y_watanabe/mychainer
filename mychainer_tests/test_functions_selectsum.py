import unittest

import numpy

import chainer
from chainer import cuda
from chainer import gradient_check
from chainer import links
from chainer import testing
from chainer.testing import attr
from mychainer.functions.array.selectsum import select_sum


@testing.parameterize(
    {'labels': [[0, 1], [0], [5, 4, 3], [2, 1]], 'n_label': 10},
    {'labels': [[0, 99], [0, 10], [5, 4, 3], [3, 1], [0, 0, 1]], 'n_label': 100},   # allow duplicate label
)
class TestSelectSum(unittest.TestCase):
    def setUp(self):
        batchsize = len(self.labels)

        self.x_data = numpy.random.uniform(
            -1, 1, (batchsize, self.n_label)).astype(numpy.float32)
        self.t_data = [numpy.array(l, dtype=numpy.int32) for l in self.labels]
        self.gy_data = numpy.random.uniform(
            -1, 1, (batchsize, )).astype(numpy.float32)
        self.check_backward_options = {}

    def check_forward(self, x_data, t_data):
        x = chainer.Variable(x_data)
        t = [chainer.Variable(ti_data) for ti_data in t_data]
        y = select_sum(x, t)
        #from IPython import embed; embed()
        y_exp = numpy.array([cuda.to_cpu(x_data)[k, ti].sum() for (k, ti) in enumerate(self.t_data)],
                            dtype=numpy.float32)
        numpy.testing.assert_equal(cuda.to_cpu(y.data), y_exp)

    def test_forward_cpu(self):
        self.check_forward(self.x_data, self.t_data)


    @attr.gpu
    def test_forward_gpu(self):
        self.check_forward(cuda.to_gpu(self.x_data),
                           [cuda.to_gpu(ti) for ti in self.t_data])

    def check_backward(self, x_data, t_data, gy_data):
        args = tuple([x_data,] + t_data)

        def f(*inputs):
            x = inputs[0]
            t = inputs[1:]
            y = select_sum(x, t)
            return (y,)

        gradient_check.check_backward(
            f, args, gy_data, eps=0.01, **self.check_backward_options)

    def test_backward_cpu(self):
        self.check_backward(self.x_data, self.t_data, self.gy_data)

    @attr.gpu
    def test_backward_gpu(self):
        self.check_backward(cuda.to_gpu(self.x_data),
                            [cuda.to_gpu(ti) for ti in self.t_data],
                            cuda.to_gpu(self.gy_data))


testing.run_module(__name__, __file__)
