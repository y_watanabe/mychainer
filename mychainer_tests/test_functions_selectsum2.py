import unittest
import functools

import numpy

import chainer
from chainer import cuda
from chainer import gradient_check
from chainer import links
from chainer import testing
from chainer.testing import attr
from mychainer.functions.array.selectsum import select_sum, select_sum2


@testing.parameterize(
    {'x_lens': [7, 8, 3, 4], 't_lens': [2, 3, 4, 2]},
    {'x_lens': [5, 2, 3, 4], 't_lens': [1, 1, 6, 3]},
    # t_lens[i]: number of entities in sample i (i=0,...,batchisize-1)
)
class TestSelectSum2(unittest.TestCase):
    def setUp(self):
        batchsize = len(self.x_lens)
        assert(len(self.x_lens) == len(self.t_lens))
        self.xs_data = [numpy.random.uniform(-1, 1, (l,)).astype(numpy.float32) for l in self.x_lens]
        self.tss_data = [[numpy.random.choice(range(l), numpy.random.randint(1, l+1)) for k in range(tl)] for (l, tl) in zip(self.x_lens, self.t_lens)]

        self.gy_data = [numpy.random.uniform(-1, 1, (tl,)).astype(numpy.float32) for tl in self.t_lens]
        self.check_backward_options = {}

    def check_forward(self, xs_data, tss_data):
        xs = [chainer.Variable(x_data) for x_data in xs_data]
        tss = [[chainer.Variable(tij_data) for tij_data in ti_data] for ti_data in tss_data]
        y = select_sum2(xs, tss)

        y_exp = []
        for xi, tsi in zip(xs, tss):
            y_exp.append([sum(cuda.to_cpu(xi.data)[cuda.to_cpu(tij.data)]) for tij in tsi])

        for yi, yi_exp in zip(y, y_exp):
            numpy.testing.assert_allclose(cuda.to_cpu(yi.data), numpy.array(yi_exp, dtype=numpy.float32), atol=1e-5)

    def test_forward_cpu(self):
        self.check_forward(self.xs_data, self.tss_data)

    @attr.gpu
    def test_forward_gpu(self):
        self.check_forward([cuda.to_gpu(x_data) for x_data in self.xs_data],
                           [[cuda.to_gpu(tij) for tij in ts_data] for ts_data in self.tss_data])

    def check_backward(self, xs_data, tss_data, gy_data):
        args = tuple(xs_data + functools.reduce(lambda a, b : a + b, tss_data))
        batchsize = len(xs_data)
        t_lens = list(map(len, tss_data))

        def f(*inputs):
            xs = inputs[:batchsize]
            _t = inputs[batchsize:]

            counter = 0
            tss = []
            for ti in t_lens:
                tss.append(_t[counter: counter + ti])
                counter += ti

            y = select_sum2(xs, tss)
            return y

        gradient_check.check_backward(
            f, args, gy_data, eps=0.01, **self.check_backward_options)

    def test_backward_cpu(self):
        self.check_backward(self.xs_data, self.tss_data, self.gy_data)

    @attr.gpu
    def test_backward_gpu(self):
        self.check_backward([cuda.to_gpu(x_data) for x_data in self.xs_data],
                            [[cuda.to_gpu(tij) for tij in ts_data] for ts_data in self.tss_data],
                           [cuda.to_gpu(g) for g in self.gy_data])

testing.run_module(__name__, __file__)