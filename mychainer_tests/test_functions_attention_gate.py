import unittest

import mock
import numpy

import chainer
from chainer import cuda
from chainer import gradient_check
from chainer import testing
from chainer.testing import attr
import chainer.functions as F
from mychainer.functions.connection.attention_gate import attention_gate

#chainer.cuda.get_device(1).use()


@testing.parameterize(*testing.product({
}))
class TestAttentionGate(unittest.TestCase):
    x_lengths = [100, 80, 30, 6, 7]
    z_lengths = [3, 4, 2, 7, 8]
    dim = 10
    batchsize = len(x_lengths)

    def setUp(self):
        self.xs = [numpy.random.uniform(-1, 1, (l, self.dim)).astype('f') for l in self.x_lengths]
        self.zs = [numpy.random.uniform(-1, 1, (l, self.dim)).astype('f') for l in self.z_lengths]
        self.gys = [numpy.random.uniform(-1, 1, (l, self.dim)).astype('f') for l in self.x_lengths]

    def check_forward(self, xs_data, zs_data):
        xs = [chainer.Variable(x) for x in xs_data]
        zs = [chainer.Variable(x) for x in zs_data]
        y = attention_gate(xs, zs)

        # expect
        scores = [F.softmax(F.matmul(x, z, transb=True)) for (x, z) in zip(xs, zs)]  # scores[i].shape == (x_lengths[i], z_lengths[i])
        y_expect = [F.matmul(w, z) for (w, z) in zip(scores, zs)]  # y_expect[i].shape == (x_lengths[i].shape, dim)

        for y0, y1 in zip(y_expect, y):
            testing.assert_allclose(y0.data, y1.data, atol=1e-3, rtol=1e-2)

    def test_forward_cpu(self):
        self.check_forward(self.xs, self.zs)

    @attr.gpu
    def test_forward_gpu(self):
        self.check_forward([cuda.to_gpu(x) for x in self.xs], [cuda.to_gpu(z) for z in self.zs])

    def check_backward(self, xs_data, zs_data, gys_data):
        args = tuple(xs_data + zs_data)
        grads = gys_data

        def f(*inputs):
            batchsize = len(inputs) // 2
            xs = inputs[:batchsize]
            zs = inputs[batchsize:]
            y = attention_gate(xs, zs)
            return y

        gradient_check.check_backward(
            f, args, grads, eps=1e-2, rtol=1e-3, atol=1e-3)

    def test_backward_cpu(self):
        self.check_backward(self.xs, self.zs, self.gys)

    @attr.gpu
    def test_backward_gpu(self):
        self.check_backward(list(map(lambda x: cuda.to_gpu(x), self.xs)),
                            list(map(lambda x: cuda.to_gpu(x), self.zs)),
                            list(map(lambda x: cuda.to_gpu(x), self.gys)))


testing.run_module(__name__, __file__)


