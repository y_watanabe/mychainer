import unittest

import numpy

import chainer
from chainer import cuda
from chainer import gradient_check
from chainer import links
from chainer import testing
from chainer.testing import attr
from mychainer.functions.connection.multi_softmax_loss import multi_softmax_loss


@testing.parameterize(
    {'labels': [[0, 1], [0, 5, 2], [5, 4, 3], [2, 1]], 'n_label': 10},
    {'labels': [[0, 11, 99], [0, 10], [5, 4, 3], [3, 1], [0, 0, 1]], 'n_label': 100},   # allow duplicate label
)
class TestMultiSoftmaxLoss(unittest.TestCase):
    def setUp(self):
        batchsize = len(self.labels)

        self.x_data = numpy.random.uniform(
            -1, 1, (batchsize, self.n_label)).astype(numpy.float32)
        self.ts_data = [numpy.array(l, dtype=numpy.int32) for l in self.labels]
        self.gy_data = numpy.random.uniform(-1, 1, ()).astype(numpy.float32)
        self.check_backward_options = {}

    def check_forward(self, x_data, ts_data):
        x = chainer.Variable(x_data)
        ts = [chainer.Variable(ti_data) for ti_data in ts_data]
        y = multi_softmax_loss(x, ts)

        smx = [numpy.exp(xi) / numpy.sum(numpy.exp(xi)) for xi in cuda.to_cpu(x_data)]
        y_exp0 = numpy.array([-numpy.log(smx[k][ti].sum()) for (k, ti) in enumerate(self.ts_data)],
                            dtype=numpy.float32)
        y_exp = numpy.array(numpy.mean(y_exp0), dtype=numpy.float32)
        numpy.testing.assert_allclose(cuda.to_cpu(y.data), y_exp, rtol=1e-6)

    def test_forward_cpu(self):
        self.check_forward(self.x_data, self.ts_data)

    @attr.gpu
    def test_forward_gpu(self):
        self.check_forward(cuda.to_gpu(self.x_data),
                           [cuda.to_gpu(ti) for ti in self.ts_data])

    def check_backward(self, x_data, ts_data, gy_data):
        args = tuple([x_data,] + ts_data)
        self.check_backward_options['rtol'] = 1e-4
        self.check_backward_options['atol'] = 1e-4

        def f(*inputs):
            x = inputs[0]
            ts = inputs[1:]
            y = multi_softmax_loss(x, ts)
            return (y,)

        gradient_check.check_backward(
            f, args, gy_data, eps=0.01, **self.check_backward_options)

    def test_backward_cpu(self):
        self.check_backward(self.x_data, self.ts_data, self.gy_data)

    @attr.gpu
    def test_backward_gpu(self):
        self.check_backward(cuda.to_gpu(self.x_data),
                            [cuda.to_gpu(ti) for ti in self.ts_data],
                            cuda.to_gpu(self.gy_data))

testing.run_module(__name__, __file__)